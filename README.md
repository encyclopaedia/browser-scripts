# browser-scripts

Greasemonkey/Tampermonkey scripts to make browsing pleasurable


### References
1. [Tampermonkey](https://www.tampermonkey.net/)
1. [Userscript Zone](https://www.userscript.zone/)
1. [Greasy Fork](https://greasyfork.org/en)
1. [Openuser JS](https://openuserjs.org/)
1.
