// ==UserScript==
// @name         TOI user style
// @namespace    https://gitlab.com/encyclopaedia
// @version      0.1
// @description  Make TOI readable
// @author       encyclopaedia
// @include      https://timesofindia.indiatimes.com/*
// @icon         https://www.google.com/s2/favicons?domain=indiatimes.com
// @require      https://code.jquery.com/jquery-3.6.0.slim.min.js
// @updateURL    https://gitlab.com/encyclopaedia/browser-scripts/-/raw/main/scripts/timesofindia.user.js
// @run-at       document-start
// ==/UserScript==

var hiddenElems = [
    ".popupWrapper",
    ".GJZvJ",
    "._1cqsl ",
    "._2cUiQ ",
    "._2kmUa ",
    ".nextASwidget",
    ".vX3bQ",
];

async function applyStyle() {
    // hides unnecessary elements
    $.each(hiddenElems, function(i, item){
        $(item).hide();
    });
}

async function readerView() {
    // applies readability view by expanding the width of elements
    $(".innerbody").width("100%");
    $(".TFt6P").width("100%");
}

async function removePaidArticles() {
    $("i.Cu-Zq").closest("div.col_l_6").hide();
}

$(document).ready(function() {
    applyStyle();
    readerView();
    removePaidArticles();
});
