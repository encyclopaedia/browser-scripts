// ==UserScript==
// @name         Youtube Adguard
// @namespace    https://gitlab.com/encyclopaedia
// @version      0.1
// @description  Block annoying youtube ads on mobile
// @author       encyclopaedia
// @match        https://m.youtube.com/*
// @icon         https://www.google.com/s2/favicons?domain=youtube.com
// @updateURL    https://gitlab.com/encyclopaedia/browser-scripts/-/raw/main/scripts/myoutube.user.js
// ==/UserScript==

'use strict';
const url = 'https://raw.githubusercontent.com/AdguardTeam/BlockYouTubeAdsShortcut/master/dist/index.js';

// Downloads the shortcut code from GitHub and runs it.
async function runShortcut() {
    try {
        const response = await fetch(url);
        const text = await response.text();
        try {
            // eslint-disable-next-line no-eval
            eval(text);
        } catch (ex) {
            completion(ex);
        }
    } catch (ex) {
        completion(ex.toString());
    }
}

runShortcut();
