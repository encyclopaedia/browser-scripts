// ==UserScript==
// @name         YT Downloader
// @namespace    https://gitlab.com/encyclopaedia
// @version      0.1
// @description  YT video download
// @author       encyclopaedia
// @include      https://www.youtube.com/*
// @include      https://m.youtube.com/*
// @require      https://code.jquery.com/jquery-3.6.0.min.js
// @updateURL    https://gitlab.com/encyclopaedia/browser-scripts/-/raw/main/scripts/youtubedown.user.js
// @grant        none
// ==/UserScript==

function downloadLink() {
    let host = document.location.href.replace('www.', '');
    let vidIdRegex = `v=(\\w{8,14})`;
    let vidId = host.match(vidIdRegex)[1];
    return "https://upull.me/#" + host
  }
  
  $(document).ready(function() {
    let downloadImg = `
    <img src="https://www.firesafetytraining.co.uk/images/firesafety/download.png" width="25" height="25">
    </img>
    `
    let dImg = $(downloadImg).click(function() {
      window.open(downloadLink(), '_blank');
    });
    dImg.parent().css('text-align','center');
  
    setTimeout(function() {
      $("div#menu-container").before(dImg);
      $("div.slim-video-action-bar-actions").prepend(dImg);
    }, 5000);
  });